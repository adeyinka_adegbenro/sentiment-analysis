import os
import psycopg2
import trainer
from main_app import db
import time
from models import Candidates
import pickle
import location

model = pickle.load(open("model.pkl", "r"))
# run migration functions on database one at a time


def updateDatabaseSentiment():
    COUNT = 5243
    id = 1
    map = {0: 'positive', 1: 'negative', 2: 'neutral'}
    while COUNT:
        print 'id {} , COUNT {}'.format(id, COUNT)
        sentiment = None
        candidate = db.session.query(Candidates).filter_by(id=id).one()
        full_text = candidate.full_text
        text = candidate.text
        if full_text:
            sentiment = model.predict([full_text])[0]
        elif text:
            sentiment = model.predict([text])[0]

        sentiment = map[sentiment]
        if sentiment:
            candidate.sentiment = sentiment
            db.session.commit()
        id += 1
        COUNT = COUNT - 1


def preProcessAllText():
    # re clean all tweet text in database
    start = time.clock()
    COUNT = 5243
    id = 1
    while COUNT:
        print 'Processed so far: {} | Unprocessed rows: {}'.format(id, COUNT)
        candidate = db.session.query(Candidates).filter_by(id=id).one()
        full_text = candidate.full_text
        text = candidate.text
        main_text = full_text if full_text else text
        processed_text = [trainer.pre_process_tweet(main_text)]
        # normalized_corpus expects a list
        normalized = trainer.normalize_corpus(processed_text)[0]
        candidate.normalized_text = normalized
        db.session.commit()
        id += 1
        COUNT = COUNT - 1
    print 'Duration: {}'.format(time.clock() - start)


def updateDatabaseLocation():
    COUNT = 54823
    id = 1
    while COUNT:
        print 'id {} , COUNT {}'.format(id, COUNT)
        candidate = db.session.query(Candidates).filter_by(id=id).first()
        if not candidate:
            id += 1
            COUNT = COUNT - 1
            continue
        tweet_location = location.get_states(candidate.location)
        # update new location
        candidate.location = tweet_location
        db.session.commit()
        id += 1
        COUNT = COUNT - 1


# query to update location, text and full_text column with that from tweet object
''' UPDATE Candidates
    SET location = (tweet -> 'user' -> 'location');
'''

'''UPDATE Candidates
    SET text = (tweet -> 'text'), full_text = (tweet -> 'full_text');
'''


if __name__ == '__main__':
    updateDatabaseLocation()
