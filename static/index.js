(function () {
    'use strict';
  
    var app = angular.module('SentimentApp', []);
    app.config(['$interpolateProvider', function($interpolateProvider) {
        $interpolateProvider.startSymbol('[[');
        $interpolateProvider.endSymbol(']]');
    }]);

    app.controller('SentimentController', ['$scope', '$log', '$http',
      function($scope, $log, $http) {
        var colorGroups = [
          ['#2D5F73', '#538EA6', '#F2D1B3', '#F2B8A2', '#F28C8C'],
          ['#2c3e50', '#E74C3C', '#ECF0F1', '#3498DB', '#2980B9'],
          ['#031727', '#126872', '#0B877D', '#18C29C', '#88F9D4'],
          ['#2E112D', '#540032', '#820333', '#C9283E', '#F0433A'],
          ['#332532', '#644d52', '#F77A52', '#FF974F', '#A49A87']
        ]

        var randomColor = function randomColor () {
          return colorGroups[Math.floor(Math.random() * colorGroups.length)]
        }  
        var getStored = function getStored () {
          $http.get('/stored').then(function (response) {
            // retrieve stored data first for performance
            var storedResults = response.data
            $scope.chartLoading =true
            console.log('storedResults', storedResults)
            $scope.chart1 = storedResults.chart1
            $scope.chart2 = storedResults.chart2
            $scope.chart3 = storedResults.chart3
            console.log($scope.chart1, $scope.chart2, $scope.chart3)
            $scope.chart4 = storedResults.chart4
            $scope.chartLoading = false
            google.charts.load('current', {
              'packages': ['bar', 'corechart', 'geochart'],
              'mapsApiKey': 'AIzaSyDUIdfbeR1K_Eq878CTz3UDPQ58Gr5US4s' 
            });
            drawChart1();
            drawChart2();
            google.charts.setOnLoadCallback(drawChart3);
            google.charts.setOnLoadCallback(drawChart4);
          })
        }
        getStored()

        $(window).resize(function () {
          drawChart1();
          drawChart2()
          google.charts.setOnLoadCallback(drawChart3);
          google.charts.setOnLoadCallback(drawChart4);
        })

        function drawChart1() {
          var chart;
          var colors = randomColor()
          AmCharts.ready(function() {
            // SERIAL CHART
            chart = new AmCharts.AmSerialChart();
            chart.dataProvider = $scope.chart1;
            chart.categoryField = "Candidate";
            chart.fontSize = 14;
            chart.startDuration = 1;
            chart.plotAreaFillAlphas = 0.2;
            // the following two lines makes chart 3D
            chart.angle = 24.03;
            chart.depth3D = 40;
        
            // AXES
            // category
            var categoryAxis = chart.categoryAxis;
            categoryAxis.gridAlpha = 0.2;
            categoryAxis.gridPosition = "start";
            categoryAxis.axisColor = "#00afbf";
            categoryAxis.axisAlpha = 1;
            categoryAxis.dashLength = 5;
        
            // value
            var valueAxis = new AmCharts.ValueAxis();
            // valueAxis.stackType = "3d"; // This line makes chart 3D stacked (columns are placed one behind another)
            valueAxis.gridAlpha = 0.2;
            valueAxis.axisColor = "#00afbf";
            valueAxis.axisAlpha = 1;
            valueAxis.dashLength = 5;
            chart.addValueAxis(valueAxis);
        
            // GRAPHS         
            // first graph
            var graph1 = new AmCharts.AmGraph();
            graph1.title = "Positive Support";
            graph1.valueField = "positive";
            graph1.type = "column";
            graph1.lineAlpha = 0;
            graph1.lineColor = colors[0];
            graph1.fillAlphas = 1;
            graph1.balloonText = "Positive support for [[category]]: [[value]]";
            chart.addGraph(graph1);
        
            // second graph
            var graph2 = new AmCharts.AmGraph();
            graph2.title = "Negative Support";
            graph2.valueField = "negative";
            graph2.type = "column";
            graph2.lineAlpha = 0;
            graph2.lineColor = colors[1];
            graph2.fillAlphas = 1;
            graph2.balloonText = "Negative support for [[category]]: [[value]]";
            chart.addGraph(graph2);

            // third graph
            var graph3 = new AmCharts.AmGraph();
            graph3.title = "Neutral Support";
            graph3.valueField = "neutral";
            graph3.type = "column";
            graph3.lineAlpha = 0;
            graph3.lineColor = colors[2];
            graph3.fillAlphas = 1;
            graph3.balloonText = "Neutral support for [[category]]: [[value]]";
            chart.addGraph(graph3);
          
            // draw chart
            chart.write("chartdiv");
          });
          // var chart = AmCharts.makeChart("chartdiv2",{
          //   "type": "serial",
          //   "categoryField": "Candidate",
          //   "startDuration": 1,
          //   "plotAreaFillAlphas": 0.2,
          //   "angle":24.03,
          //   "depth3D": 40,
          //   "graphs": [
          //     {
          //       "valueField": "positive",
          //       "type": "column",
          //       "lineAlpha": 0,
          //       "lineColor": colors[0],
          //       "fillAlphas": 1,
          //       "balloonText": "Positive support for [[category]]: [[value]]"
          //     },
          //     {
          //       "valueField": "negative",
          //       "type": "column",
          //       "lineAlpha": 0,
          //       "lineColor": colors[1],
          //       "fillAlphas": 1,
          //       "balloonText": "Negative support for [[category]]: [[value]]"
          //     },
          //     {
          //       "valueField": "neutral",
          //       "type": "column",
          //       "lineAlpha": 0,
          //       "lineColor": colors[2],
          //       "fillAlphas": 1,
          //       "balloonText": "Neutral support for [[category]]: [[value]]"
          //     }
          //   ],
          //   "dataProvider": $scope.chart1
          // });
        }

        function drawChart2() {
          console.log($scope.chart5)
          AmCharts.makeChart( "mapdiv", {
            /**
             * this tells amCharts it's a map
             */
            "type": "map",
          
            /**
             * create data provider object
             * map property is usually the same as the name of the map file.
             * getAreasFromMap indicates that amMap should read all the areas available
             * in the map data and treat them as they are included in your data provider.
             * in case you don't set it to true, all the areas except listed in data
             * provider will be treated as unlisted.
             */
            "dataProvider": {
              "map": "nigeriaLow",
              "areas": $scope.chart2,
              "getAreasFromMap": false
            },
          
            /**
             * create areas settings
             * autoZoom set to true means that the map will zoom-in when clicked on the area
             * selectedColor indicates color of the clicked area.
             */
            "areasSettings": {
              "autoZoom": true,
              "selectedColor": "#CC0000"
            },
          
            /**
             * let's say we want a small map to be displayed, so let's create it
             */
            "smallMap": {}
          } );
        }

        function drawChart3 () {
          console.log('data3', $scope.chart3)
          var data = new google.visualization.arrayToDataTable($scope.chart3);
          var options = {
            title: 'Candidate Support',
            curveType: 'none',
            legend: { position: 'bottom' },
            hAxis: {
              title: 'Time'
            },
            vAxis: {
              title: 'Popularity'
            },
            fontName: 'Quicksand sans-serif'
          }
          var chart = new google.visualization.LineChart(document.getElementById('curve_chart'));
          chart.draw(data, options);
        }

        function drawChart4() {
          console.log('row 4', $scope.chart4)
          var data = google.visualization.arrayToDataTable($scope.chart4);
          var options_fullStacked = {
            isStacked: 'percent',
            height: 300,
            legend: { position: 'top', maxLines: 3 },
            hAxis: {
              minValue: 0,
              ticks: [0, .3, .6, .9, 1]
            },
            fontName: 'Quicksand sans-serif'
          };


          var chart = new google.visualization.BarChart(document.getElementById('stacked_chart'));
          chart.draw(data, options_fullStacked);

        }


        function get_new_tweets  () {
          $http.get('/select_tweets').then(function (response) {
            console.log('select tweets', response)
            var Tweets = response.data
            if (!Tweets) {
              return
            }
            $('#tweetContainer').empty();
            var tweetContainer = document.getElementById('tweetContainer')
            for (var i=0; i < Tweets.length; i++) {
              var tweet = document.createElement("div")
              tweetContainer.appendChild(tweet)
              // console.log(tweet, tweetContainer)
              if (!Tweets[i][0]) { continue }
              var id = Tweets[i][0].id_str
              // console.log('id', id)
              twttr.widgets.createTweet(
                id, tweet, 
                {
                  conversation : 'none',    // or all
                  cards        : 'hidden',  // or visible 
                  linkColor    : '#cc0000', // default is blue
                  theme        : 'light'    // or dark
                })
              .then (function (el) {
              })
            }
            /* $('#tweetContainer').cycle({
              fx:     'scrollUp', 
              timeout: 6000, 
              delay:  -2000 
            }); */
          }).catch(function (err) {
            console.log(err)
          })
        }
        var getResults = function() {
          // this function gets latest data from
          // the server and updates the dom
          var url_results = '/results'
          $http.get(url_results).then(function (resp) {
            var allResults = resp.data
            // then query for newer data
            console.log('allResults', allResults)
            $scope.chart1 = allResults.chart1
            $scope.chart2 = allResults.chart2
            $scope.chart3 = allResults.chart3
            $scope.chart4 = allResults.chart4
          }).catch(function (error) {
            console.log(error)
          })
          get_new_tweets()
          // refresh the dom every two minutes
          // window.setInterval(get_new_tweets, 60000)
        };

        var select = function select() {
          $http.get('/get_scroll').then(function (response) {
            console.log('/get_scroll', response)
            $scope.loading = true
            $('#tweetContainer').empty();
            var Tweets = response.data
            var tweetContainer = document.getElementById('tweetContainer')
            for (var i=0; i < Tweets.length; i++) {
              var tweet = document.createElement("div")
              tweetContainer.appendChild(tweet)
              // console.log('tweet', Tweets[i][0])
              if (!Tweets[i][0]) { continue }
              var id = Tweets[i][0].id_str
              // console.log('id', id)
              twttr.widgets.createTweet(
                id, tweet, 
                {
                  conversation : 'none',    // or all
                  cards        : 'hidden',  // or visible 
                  linkColor    : '#cc0000', // default is blue
                  theme        : 'light'    // or dark
                })
              .then (function (el) {
                // console.log('tweetContainer & el', tweetContainer, el)
              })
            }
            $('#tweetContainer').cycle({
              fx:     'scrollUp', 
              timeout: 6000, 
              delay:  -50000 
            });
            setTimeout(scrollDiv_init(), 2000)
          })
          getResults()
        }
        select()
        var DivElmnt;
        var ReachedMaxScroll, PreviousScrollTop;
        var ScrollInterval, ScrollRate = 10;

        function scrollDiv() {
          if (!ReachedMaxScroll) {
            DivElmnt.scrollTop = PreviousScrollTop;
            
            PreviousScrollTop++;
            
            ReachedMaxScroll = DivElmnt.scrollTop >= (DivElmnt.scrollHeight - DivElmnt.offsetHeight);
          }
          else {
            ReachedMaxScroll = (DivElmnt.scrollTop == 0)?false:true;
            
            DivElmnt.scrollTop = PreviousScrollTop;
            PreviousScrollTop--;
          }
        }

        function scrollDiv_init() {
          DivElmnt = document.getElementById('tweetContainer');
          ReachedMaxScroll = false;
          
          DivElmnt.scrollTop = 0;
          PreviousScrollTop  = 0;
          
          ScrollInterval = setInterval(scrollDiv, ScrollRate);
        }
                
        function pauseDiv() {
          clearInterval(ScrollInterval);
        }
        
        function resumeDiv() {
          PreviousScrollTop = DivElmnt.scrollTop;
          ScrollInterval    = setInterval(scrollDiv, ScrollRate);
        }

        window.addEventListener('load', function () {
          // scrollDiv_init();
        })
      }
    ])
  }())