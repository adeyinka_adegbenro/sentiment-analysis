## Setup

 1. Install Python packages:

    pip install -t lib -r requirements.txt

2. Install Postgresql

3. Start the server by running gunicorn  --bind  0.0.0.0:5000 app and visit 0.0.0.0:5000/
