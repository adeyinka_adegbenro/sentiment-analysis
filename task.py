from tweepy import OAuthHandler
from tweepy import Stream
from tweepy.streaming import StreamListener
from psycopg2.extras import Json, DictCursor
from config import Config
from main_app import db
from models import Candidates
from datetime import datetime
from config import Config
import trainer
import psycopg2
import tweepy
import logging
import time
import os
import json
import pprint
import pickle
import location
consumer_key = Config.CONSUMER_KEY
consumer_secret = Config.CONSUMER_SECRET
access_token = Config.ACCESS_TOKEN
access_secret = Config.ACCESS_SECRET
auth = OAuthHandler(consumer_key, consumer_secret)
auth.set_access_token(access_token, access_secret)
api = tweepy.API(auth, wait_on_rate_limit=True)
#model = pickle.load(open("/var/www/Sentiment_Analysis/model.pkl", "r"))
model = pickle.load(open("model.pkl", "r"))
sentiment_map = {0: 'positive', 1: 'negative', 2: 'neutral'}


class MyListener(StreamListener):
    def __init__(self):
        self.data_count = 0

    def on_data(self, data):
        tweet = json.loads(data)
        logging.info('current data count {}'.format(self.data_count))
        if 'retweeted_status' not in tweet:
            self.data_count += 1
            text = tweet['text']
            # for tweets containing more than 180 characters
            full_text = tweet.get('full_text', None)
            # get tweet sentiment
            main_text = full_text if full_text else text
            main_text = trainer.pre_process_tweet(main_text)
            normalized_text = trainer.normalize_corpus([main_text])
            sentiment = sentiment_map[model.predict(normalized_text)[0]]
            tweet_location = location.get_states(tweet['user']['location'])
            tweet = Candidates(tweet=tweet, text=text,
                               full_text=full_text,
                               normalized_text=normalized_text[0],
                               location=tweet_location, sentiment=sentiment)
            db.session.add(tweet)
            db.session.commit()
            return True

    def on_error(self, status):
        logging.info('TweepyAPI Status {}'.format(status))
        if status == 420:
            return False


def stream():
    try:
        twitter_stream = Stream(auth, MyListener())
        twitter_stream.filter(track=['buhari', 'atiku abubakar',
                                     'tunde bakare', 'ayo fayose', 'fayose',
                                     'donald duke', 'sule lamido', 'fela durotoye',
                                     'olufunmilayo adesanya davies', 'asuu',
                                     'hamza al-mustapha', 'rabiu kwankwaso',
                                     'kingsley moghalu', 'skc ogbonnia', 'boko-haram', 'boko haram',
                                     'herdsmen', 'fulani herdsmen', 'biafra',
                                     'IPOB', 'endsars', 'hamza al mustapha', 'dapchi'])
    except Exception as e:
        logging.info(e)
        logging.error(e)
        time.sleep(15)
        # we are serving forever
        stream()

if __name__ == '__main__':
    stream()
