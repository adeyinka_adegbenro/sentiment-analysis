from sqlalchemy.dialects.postgresql import JSON
from sqlalchemy import Column, ForeignKey, Integer, String, DateTime
from sqlalchemy.dialects.postgresql import JSON
from sqlalchemy.sql import func
from main_app import db


class Candidates(db.Model):
    __tablename__ = 'candidates'
    id = Column(Integer, primary_key=True)
    tweet = Column(JSON)
    text = Column(String())
    full_text = Column(String())
    # normalized_text contains text that has been cleaned and processed for prediction
    normalized_text = Column(String())
    sentiment = Column(String())
    location = Column(String())
    timestamp = Column(DateTime(timezone=True), server_default=func.now())


class Results(db.Model):
    __tablename__ = 'results'
    id = Column(Integer, primary_key=True)
    processed_data = Column(JSON)
    modified = Column(
        DateTime(timezone=True), nullable=False, server_default=func.now())


class ScrollTweets(db.Model):
    __tablename__ = 'scrolltweets'
    id = Column(Integer, primary_key=True)
    tweets = Column(JSON)
    modified = Column(
        DateTime(timezone=True), nullable=False, server_default=func.now())
