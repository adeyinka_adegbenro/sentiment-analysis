from flask import Flask
from flask_sqlalchemy import SQLAlchemy
import os, config

app = Flask(__name__)
app.config.from_object(config.DevelopmentConfig)
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True
db = SQLAlchemy(app)
app.secret_key = 'rireifw34i8430nfgi34jrenfr3043_'
import routes