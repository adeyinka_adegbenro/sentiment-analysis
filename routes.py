from flask import Flask
from collections import OrderedDict
from flask_sqlalchemy import SQLAlchemy
from flask import render_template
from flask import jsonify
from main_app import app as application
from main_app import db
from models import Candidates, Results, ScrollTweets
from config import Config
import logging
import json
import os
import pickle
import location
import requests
import psycopg2
import task
import time
import random

db_url = Config.SQLALCHEMY_DATABASE_URconn = psycopg2.connect(db_url)
c = conn.cursor()
# model = pickle.load(open("/var/www/Sentiment_Analysis/model.pkl", "r"))
model = pickle.load(open("model.pkl", "r"))


def extract_candidate_tweets():
    # filter tweets based on whether candidate was mentioned in tweet
    start = time.clock()
    locations = []
    # extract buhari tweets
    c.execute('SELECT normalized_text, location, timestamp FROM candidates WHERE text LIKE \'%buhari%\' AND sentiment = \'positive\';')
    buhari_positive = c.fetchall()
    #print 'buhari_positive', buhari_positive
    c.execute('SELECT normalized_text, sentiment FROM candidates WHERE text LIKE \'%buhari%\' AND sentiment = \'negative\';')
    buhari_negative = c.fetchall()
    c.execute('SELECT normalized_text, sentiment FROM candidates WHERE text LIKE \'%buhari%\' AND sentiment = \'neutral\';')
    buhari_neutral = c.fetchall()
    # buhari = [x[0] for x in tweets if 'buhari' in x[0].lower()]

    # extract atiku tweets
    c.execute('SELECT normalized_text, location, timestamp FROM candidates WHERE text LIKE \'%atiku%\' AND sentiment = \'positive\';')
    atiku_positive = c.fetchall()
    c.execute('SELECT normalized_text, sentiment FROM candidates WHERE text LIKE \'%atiku%\' AND sentiment = \'negative\';')
    atiku_negative = c.fetchall()
    c.execute('SELECT normalized_text, sentiment FROM candidates WHERE text LIKE \'%atiku%\' AND sentiment = \'neutral\';')
    atiku_neutral = c.fetchall()
    # atiku = [x[0] for x in tweets if 'atiku' in x[0].lower()]

    # extract Bakare tweets
    c.execute('SELECT normalized_text, location, timestamp FROM candidates WHERE text LIKE \'%bakare%\' AND sentiment = \'positive\';')
    bakare_positive = c.fetchall()
    c.execute('SELECT normalized_text, sentiment FROM candidates WHERE text LIKE \'%bakare%\' AND sentiment = \'negative\';')
    bakare_negative = c.fetchall()
    c.execute('SELECT normalized_text, sentiment FROM candidates WHERE text LIKE \'%bakare%\' AND sentiment = \'neutral\';')
    bakare_neutral = c.fetchall()
    # bakare = [x[0] for x in tweets if 'bakare' in x[0].lower()]

    # extract Fayose Tweets
    c.execute('SELECT normalized_text, location, timestamp FROM candidates WHERE text LIKE \'%fayose%\' AND sentiment = \'positive\';')
    fayose_positive = c.fetchall()
    c.execute('SELECT normalized_text, sentiment FROM candidates WHERE text LIKE \'%fayose%\' AND sentiment = \'negative\';')
    fayose_negative = c.fetchall()
    c.execute('SELECT normalized_text, sentiment FROM candidates WHERE text LIKE \'%fayose%\' AND sentiment = \'neutral\';')
    fayose_neutral = c.fetchall()

    # extract Durotoye
    c.execute('SELECT normalized_text, location, timestamp FROM candidates WHERE text LIKE \'%durotoye%\' AND sentiment = \'positive\';')    
    durotoye_positive = c.fetchall()
    c.execute('SELECT normalized_text, sentiment FROM candidates WHERE text LIKE \'%durotoye%\' AND sentiment = \'negative\';')    
    durotoye_negative = c.fetchall()
    c.execute('SELECT normalized_text, sentiment FROM candidates WHERE text LIKE \'%durotoye%\' AND sentiment = \'neutral\';')    
    durotoye_neutral = c.fetchall()
    # fayose = [x[0] for x in tweets if 'buhari' in x[0].lower()]
    # duke = []
    # lamido = []
    # adesanya = []
    # almustapha = []
    # durotoye = [x for x in tweets if 'durotoye' in x[0].lower()]
    # kwankwaso = []
    # moghalu = []
    # ogbonnia = []
    duration = time.clock() - start
    # print'extract_candidate_tweets ', duration
    return {
            'Buhari': {'positive': buhari_positive,
                       'negative': buhari_negative, 'neutral': bakare_neutral},
            'Atiku': {'positive': atiku_positive, 'negative': atiku_negative,
                      'neutral': atiku_neutral},
            'Bakare': {'positive': bakare_positive, 'negative':
                       bakare_negative, 'neutral': bakare_neutral},
            'Fayose': {'positive': fayose_positive,
                       'negative': fayose_negative, 'neutral': fayose_neutral},
            'Durotoye': {'positive': durotoye_positive,
                         'negative': durotoye_negative,
                         'neutral': durotoye_neutral}
            }


candidate_tweets = extract_candidate_tweets()

@application.route("/stored", methods=['GET'])
def get_data_stored():
    results = db.session.query(Results).get(1)
    stored = results.processed_data
    # get the previous stored chart data
    return json.dumps(stored)


@application.route("/results", methods=['GET'])
def get_results():
    start = time.time()
    data = {}  
    issue_tweets = extract_issues_tweets()
    sentiments = chart1_data()
    locations = chart2_data()
    time_series = chart3_data()
    data['chart1'] = sentiments
    data['chart2'] = locations
    data['chart3'] = time_series
    data['chart4'] = issue_tweets
    results = db.session.query(Results).get(1)
    if not results:
        results = Results()
    # print 'rRESULTS', results
    # update the previous stored data with new one
    results.processed_data = data
    db.session.add(results)
    db.session.commit()
    duration = time.time() - start
    print "get_results duration=", duration
    # print "results from get_results", data
    return json.dumps(data)


@application.route('/')
@application.route('/index')
def index():
    return render_template('index.html')


@application.route('/get_scroll')
def stored_scroll():
    # retrieve stored tweets to load page
    # while /select_tweets gets the latest tweet
    # from db
    scrolltweets = db.session.query(ScrollTweets).get(1)
    tweets = scrolltweets.tweets
    return json.dumps(tweets)


@application.route('/select_tweets')
def select_tweets():
    # get the tweets and store it
    # for next request to be retrieved in
    # /get_scroll, to improve page load.
    # on success, this updates the dom with the latest tweets
    c.execute('select tweet from candidates ORDER BY RANDOM() limit 50')
    tweets = c.fetchall()
    stored_tweets = db.session.query(ScrollTweets).get(1)
    if not stored_tweets:
        stored_tweets = ScrollTweets()
    stored_tweets.tweets = tweets
    db.session.add(stored_tweets)
    db.session.commit()
    # print('tweets in select_tweets', tweets)
    return json.dumps(tweets)


def chart1_data():
    '''prepare data that goes in chart one on landing page'''
    '''prepare data that goes in chart one on landing page'''
    start = time.clock()

    # get aggregate of positive, negative and neutral tweets per candidate
    sentiments = [{'Candidate': candidate, 'positive': len(value['positive']), 'negative': len(value['negative']),
                  'neutral': len(value['neutral'])} for candidate, value in
                  candidate_tweets.items()]
    # TODO: Use Sql to generate the count of positive tweets per candidates, instead of python as that will be faster
    # sentiments.insert(0, ['Candidates', 'Positive', 'Negative', 'Neutral'])

    duration = time.clock() - start
    print'chart1_data duration ', duration
    return sentiments


'''def chart2_data():
    print 'chart2 data'
    start = time.clock()
    locations = []
    # get positive tweets per candidate
    candidate_sentiments = {candidate: value['positive'] for candidate, value in candidate_tweets.items()}
    locationAggregate = location.getData(candidate_sentiments)
    print 'D', locationAggregate
    for place in locationAggregate:
        # get the candidates with the most support in state
        max_candidate = max(locationAggregate[place], key=lambda key: locationAggregate[place][key])
        distr = [place.title(), max_candidate]
        locations.append(distr)
    # print('locations', locations)
    duration = time.clock() - start
    # print'chart2_data duration ', duration
    return locations'''


def chart3_data():
    start = time.clock()
    # TODO: shorten chart data to only the ten most recent weeks
    chart_data = [['Date', 'Buhari', 'Atiku', 'Bakare', 'Fayose', 'Durotoye']]
    timed_tweets = OrderedDict()
    for candidate, tweets in candidate_tweets.items():
        candidate_sentiments = candidate_tweets[candidate]['positive']
        for tweet in candidate_sentiments:
            timestamp = (tweet[2]).strftime('%d/%b/%Y')
            print 'time', timestamp
            if timestamp not in timed_tweets:
                timed_tweets[timestamp] = OrderedDict(
                    [('Buhari', 0), ('Atiku', 0), ('Bakare', 0),
                     ('Fayose', 0), ('Durotoye', 0)])
                timed_tweets[timestamp][candidate] = 1
            elif candidate not in timed_tweets[timestamp]:
                timed_tweets[timestamp][candidate] = 1
            else:
                timed_tweets[timestamp][candidate] += 1

    for timestamp in timed_tweets:
        data = [v for k, v in timed_tweets[timestamp].items()]
        data.insert(0, timestamp)
        chart_data.append(data)
    print 'chart 3 duration', time.clock() - start
    return chart_data


def chart2_data():
    import state_codes
    start = time.clock()
    areas = []
    # #0000FF means APC, #ff0000 means PDP, FFFF00 means ANN, Alliance for New Nigeria,
    # GPN : purple
    party_color_code = {'Buhari': '#0000FF', 'Atiku': '#ff0000', 'Bakare': 'grey',
                        'Duke': '#ff0000', 'Lamido': '#ff0000',
                        'Adesanya': 'grey', 'Ogbonnia':  '#0000FF', 
                        'Durotoye': 'FFFF00', 'kwankwaso': '#ff0000', 
                        'Al-Mustapha': 'purple', 'Fayose': '#ff0000'}
    # get positive tweets per candidate
    candidate_sentiments = {candidate: value['positive'] for candidate, value in candidate_tweets.items()}
    locationAggregate = location.getData(candidate_sentiments)
    print 'D', locationAggregate
    for place in locationAggregate:
        # get the candidates with the most support in state
        max_candidate = max(locationAggregate[place], key=lambda key: locationAggregate[place][key])
        party_color = party_color_code[max_candidate]
        # distr = [place.title(), max_candidate]
        distr = {'id': state_codes.states[place.title()], 'color': party_color, 
                 'title': 'State: {}<br>Candidate with most support: {}'.format(place.title(), max_candidate)}
        areas.append(distr)
    # print('locations', locations)
    duration = time.clock() - start
    # print'chart2_data duration ', duration
    return areas


def extract_issues_tweets():
    # determine  candidate support on issues
    start = time.clock()
    issues = ['asuu', 'nasu', 'jamb', 'waec', 'boko-haram', 'biafra', 'ipob',
              'sars', 'fulani herdsmen', 'phcn']
    candidates = candidate_tweets.keys()
    # get the positive tweets about each candidate
    candidates_positive = {k: v['positive'] for k, v in candidate_tweets.items()}
    candidates.insert(0, 'Issues')
    # print candidates_positive
    chartList = []
    # prepare chart data
    for issue in issues:
        issueList = [issue]
        for candidate, positive_tweets in candidates_positive.items():
            if not positive_tweets:
                filtered_issue_count = 0
            else:
                # filter the candidate's positive tweets by checking if
                # issue was mentioned in tweet
                filtered_issue_count = len(
                    filter(
                        lambda x: any(j in x for j in issue),
                        positive_tweets))
            issueList.append(filtered_issue_count)
        # append empyty string for google charts usage
        issueList.append('')
        chartList.append(issueList)
    candidates.append({'role': 'annotation'})
    chartList.insert(0, candidates)
    # print chartList
    duration = time.clock() - start
    # print'extract_issues_tweets ', duration
    return chartList


if __name__ == '__main__':
    application.run(host='0.0.0.0', port=5000)