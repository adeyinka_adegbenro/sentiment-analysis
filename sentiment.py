import re
from textblob import TextBlob
import json


class TwitterClient(object):
    '''
    Generic Twitter Class for sentiment analysis.
    '''
    def __init__(self):
        '''
        Class constructor or initialization method.
        '''
        # keys and tokens from the Twitter Dev Console
        consumer_key = '5A4Ca4hytUcQKB0yPOzRmJFFl'
        consumer_secret = 'dzYbfnMfW3Ad8Y81gt2qGSnaKyoLBQNGzNVyAvtjf7b1Rt8fsH'
        access_token = '471100010-Jnd7FoXSNbTxm2AYdlFOWAJHUmT4S57wxRIh12wp'
        access_token_secret = 'iIDfwho2semphlwcpPhhnHqWbuB6dx7E543j2AdjKVMrc'

        # attempt authentication
        try:
            # create OAuthHandler object
            self.auth = OAuthHandler(consumer_key, consumer_secret)
            # set access token and secret
            self.auth.set_access_token(access_token, access_token_secret)
            # create tweepy API object to fetch tweets
            self.api = tweepy.API(self.auth)
        except:
            print("Error: Authentication Failed")

    def clean_tweet(self, tweet):
        '''
        Utility function to clean tweet text by
        removing links, special characters
        using simple regex statements.
        '''
        return ' '.join(re.sub("(@[A-Za-z0-9]+)|([^0-9A-Za-z \t])\
                                    |(\w+:\/\/\S+)", " ", tweet).split())

    def get_tweet_sentiment(self, tweet):
        '''
        Utility function to classify sentiment of passed tweet
        using textblob's sentiment method
        '''
        # create TextBlob object of passed tweet text
        analysis = TextBlob(self.clean_tweet(tweet))
        # set sentiment
        if analysis.sentiment.polarity > 0:
            return 'positive'
        elif analysis.sentiment.polarity == 0:
            return 'neutral'
        else:
            return 'negative'

    def get_tweets(self, fetched_tweets):
        '''
        Main function to fetch tweets and parse them.
        '''

        try:
            # parsing tweets one by one
            sentiments = []
            for i in range(len(fetched_tweets)):
                # empty dictionary to store required params of a tweet
                tweet = fetched_tweets[i][0]

                # saving text of tweet
                # saving sentiment of tweet
                sentiments.append(self.get_tweet_sentiment(tweet))

            # return parsed tweets
            return sentiments

        except tweepy.TweepError as e:
            # print error (if any)
            print("Error : " + str(e))


def clean_tweet(tweet):
    '''
    Utility function to clean tweet text by
    removing links, special characters
    using simple regex statements.
    '''
    return ' '.join(re.sub("(@[A-Za-z0-9]+)|([^0-9A-Za-z \t])\
                                |(\w+:\/\/\S+)", " ", tweet).split())


def get_tweet_sentiment(tweet):
    '''
    Utility function to classify sentiment of passed tweet
    using textblob's sentiment method
    '''
    # create TextBlob object of passed tweet text
    analysis = TextBlob(clean_tweet(tweet))
    # set sentiment
    if analysis.sentiment.polarity > 0:
        return 'positive'
    elif analysis.sentiment.polarity == 0:
        return 'neutral'
    else:
        return 'negative'


def get_tweets(fetched_tweets):
    '''
    Main function to fetch tweets and parse them.
    '''
    # print 'types', type(fetched_tweets), fetched_tweets
    # parsing tweets one by one
    sentiments = []
    for i in range(len(fetched_tweets)):
        # empty dictionary to store required params of a tweet
        tweet = fetched_tweets[i][0]

        # saving text of tweet
        # saving sentiment of tweet
        sentiments.append(get_tweet_sentiment(tweet))

    # return parsed tweets
    return sentiments


def getData(fetched_tweets, candidate):
    # creating object of TwitterClient Class
    sentiments = get_tweets(fetched_tweets)
    pos = 0
    neg = 0
    neu = 0
    for sentiment in sentiments:
        if sentiment == 'positive':
            pos += 1
        elif sentiment == 'negative':
            neg += 1
        elif sentiment == 'neutral':
            neu += 1
    return [candidate, pos, neg, neu]


def getPositive(fetched_tweets):
    sentiments = get_tweets(fetched_tweets)
    positive_tweets = []
    i = 0
    for sentiment in sentiments:
        if sentiment == 'positive':
            positive_tweets.append(fetched_tweets[i])
        i += 1
    return positive_tweets


def main(file):
    getData(file)
if __name__ == "__main__":
    # calling main function
    main('buhari_3028-58-20.json')