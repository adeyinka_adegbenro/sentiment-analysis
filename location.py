import json
import re
import requests
import pprint
filenames = ['atiku_2018-06-21.json', 'buhari_2018-11-21.json']
locations = {}
states = {
    'abia': 'Umuahia', 'adamawa': 'Yola',
    'akwa Ibom': 'Uyo', 'anambra': 'Awka', 
    'bauchi': 'Bauchi', 'bayelsa': 'Yenagoa',
    'benue': 'Makurdi', 'borno': 'Maiduguri',
    'cross River': 'Calabar', 'delta': 'Asaba',
    'ebonyi': 'Abakaliki', 'edo': 'Benin',
    'ekiti': 'Ado-Ekiti', 'enugu': 'Enugu',
    'gombe': 'Gombe', 'imo': 'Owerri',
    'jigawa': 'Dutse', 'kaduna': 'Kaduna',
    'kano': 'Kano', 'katsina': 'Katsina',
    'kebbi': 'Birnin Kebbi', 'kogi': 'Lokoja',
    'kwara': 'Ilorin', 'lagos': 'Ikeja', 
    'nasarawa': 'Lafia', 'niger': 'Minna', 
    'ogun': 'Abeokuta', 'ondo': 'Akure',
    'osun': 'Oshogbo', 'oyo': 'Ibadan', 
    'plateau': 'Jos', 'rivers': 'Port Harcourt',
    'sokoto': 'Sokoto', 'taraba': 'Jalingo', 
    'yobe': 'Damaturu', 'zamfara': 'Gusau',
    'federal capital territory': 'Abuja'
    }


def find_tweet_location(tweets, candidate):
    for tweet in tweets:
        location = tweet[1]
        print 'tweets=========>', tweet[1], '\n'
        try:
            if location:
                if location not in locations:
                    locations[location] = {}
                    locations[location][candidate] = 1
                elif candidate not in locations[location]:
                    locations[location][candidate] = 1
                else:
                    locations[location][candidate] += 1
        except KeyError:
            continue


def get_states(location):
    if not location:
        return None
    location = location.lower()
    for state, capital in states.items():
        a = re.compile('\\b'+state.lower()+'\\b')
        b = re.compile('\\b'+capital.lower()+'\\b')
        if re.findall(a, location) or re.findall(b, location):
            # print( '\nfound state', state, '\n')
            return state.title()


def getData(candidates):
    t = [find_tweet_location(candidates[candidate], candidate) for candidate in candidates]
    print '\n\n\nLOCATIONS====>', locations
    return locations


def getCoordinates(location):
    import googlemaps
    from pprint import pprint
    from datetime import datetime
    gmaps = googlemaps.Client(key='AIzaSyArVvZ6DFx1qMyvVJiTN5YF2JQV61UiNNo')
    geocode_result = gmaps.geocode(location)
    return geocode_result


if __name__ == '__main__':
    getCoordinates('surulere')