from pandas import DataFrame, read_csv
from sklearn.model_selection import train_test_split
from sklearn.feature_extraction.text import CountVectorizer, TfidfTransformer
from sklearn.pipeline import Pipeline
from sklearn.linear_model import SGDClassifier
from bs4 import BeautifulSoup
from contractions import CONTRACTION_MAP
from nltk.stem import WordNetLemmatizer
from pattern.en import tag
from nltk.corpus import wordnet as wn
import numpy as np
import pandas as pd
import pickle
import os
import nltk
import string
import re

# Steps to training our Classification Model
### Load DataSets
### Text Normalization
### Feature Extraction
### Mode Training
### Model Prediction & Evaluation
### Model Deployment


stopword_list = nltk.corpus.stopwords.words('english')
wnl = WordNetLemmatizer()


def build_feature_matrix():
    # extract features
    # extract features and use supervised learning algorithmn
    # to build a predictive model
    classifier = Pipeline([('vect', CountVectorizer()),
                           ('tfidf', TfidfTransformer()),
                           ('clf', SGDClassifier(loss='hinge', penalty='l2',
                            alpha=1e-3, random_state=42,
                            max_iter=5, tol=None)),])
    classifier.fit(x_train, y_train)
    return classifier


def tokenize_text(text):
    # break sentences into words
    tokens = nltk.word_tokenize(text) 
    tokens = [token.strip() for token in tokens]
    return tokens



def remove_stopwords(text):
    # remove common words like `is, the of` e.t.c
    tokens = tokenize_text(text)
    filtered_tokens = [token for token in tokens if token not in
                       stopword_list]
    filtered_text = ' '.join(filtered_tokens)    
    return filtered_text


def remove_special_characters(text):
    # remove punctuations
    tokens = tokenize_text(text)
    pattern = re.compile('[{}]'.format(re.escape(string.punctuation)))
    filtered_tokens = filter(None, [pattern.sub('', token) for token in
                             tokens])
    filtered_text = ' '.join(filtered_tokens)
    return filtered_text


def lemmatize_text(text):
    # reduce words to their root forms
    pos_tagged_text = pos_tag_text(text)
    lemmatized_tokens = [wnl.lemmatize(word, pos_tag) if pos_tag
                         else word                    
                         for word, pos_tag in pos_tagged_text]
    lemmatized_text = ' '.join(lemmatized_tokens)
    return lemmatized_text


def expand_contractions(text, contraction_mapping):
    # expands words like e.g can't to cannot, won't to will not e.t.c
    contractions_pattern = re.compile('({})'.format(
        '|'.join(contraction_mapping.keys())),
        flags=re.IGNORECASE | re.DOTALL)

    def expand_match(contraction):
        match = contraction.group(0)
        first_char = match[0]
        expanded_contraction = contraction_mapping.get(match)\
            if contraction_mapping.get(match)\
            else contraction_mapping.get(match.lower())                      
        expanded_contraction = first_char+expanded_contraction[1:]
        return expanded_contraction
    expanded_text = contractions_pattern.sub(expand_match, text)
    expanded_text = re.sub("'", "", expanded_text)
    return expanded_text


# Annotate text tokens with POS tags
def pos_tag_text(text):
    def penn_to_wn_tags(pos_tag):
        if pos_tag.startswith('J'):
            return wn.ADJ
        elif pos_tag.startswith('V'):
            return wn.VERB
        elif pos_tag.startswith('N'):
            return wn.NOUN
        elif pos_tag.startswith('R'):
            return wn.ADV
        else:
            return None
    tagged_text = tag(text)
    tagged_lower_text = [(word.lower(), penn_to_wn_tags(pos_tag))
                         for word, pos_tag in
                         tagged_text]
    return tagged_lower_text


def pre_process_tweet(text):
    # decode HTML to general text
    # e.g '&amp;' to '&'
    decoded = BeautifulSoup(text, 'lxml')
    decoded_text = decoded.get_text()
    pat1 = r'@[A-Za-z0-9_]+'  # for removing @user
    pat2 = r'https?://[^ ]+'  # for removing links
    combined_pattern = r'|'.join((pat1, pat2))
    # remove @user & remove links
    text = re.sub(combined_pattern, '', decoded_text)
    try:
        # remove stuff like this --> \xef\xbf\xbdll aka `utf-8-bom`
        # replace u"\ufffd" with '?'
        clean_text = text.decode('utf-8-sig').replace(u"\ufffd", '?')
    except:
        clean_text = text
    # remove pound sign and leave text
    text = re.sub("[^a-zA-Z]", " ", clean_text)

    return text.lower()


def normalize_corpus(corpus, tokenize=False):
    # normalizes our text data for easier feature extraction
    normalized_corpus = []
    for text in corpus:
        text = expand_contractions(text, CONTRACTION_MAP)
        text = lemmatize_text(text)
        text = remove_special_characters(text)
        text = remove_stopwords(text)
        normalized_corpus.append(text)
        if tokenize:
            text = tokenize_text(text)
            normalized_corpus.append(text)
    return normalized_corpus


def load_dataset(file, sep='\t'):
    # load dataset with pandas
    df = pd.read_csv(file, sep=sep)
    return df


def prepare_datasets(corpus, labels, test_data_proportion=0.3):
    train_X, test_X, train_Y, test_Y = train_test_split(corpus, labels,
                                                        test_size=0.33,
                                                        random_state=42)
    normalized_train = normalize_corpus(train_X)
    normalized_test = normalize_corpus(test_X)
    return normalized_train, normalized_test, train_Y, test_Y


'''df = read_dataset('tweets.csv')
x = df['tweet']
y = df['sentiment']

# split dataset
x_train, x_test, y_train, y_test = x[:2842], x[2842:], y[:2842], y[2842:]

classifier = build_feature_matrix()

# serialize  model to file, for import into flask app
pickle.dump(classifier, open('model.pkl', 'wb'))

# Evaluating performance on test set
# normalize testing data
# extract features using pipeline
# evaluate model performance
predicted = classifier.predict(x_train)
print np.mean(predicted == y_train)'''


if __name__ == '__main__':
    '''t = pre_process_tweet("\ud83d\ude02\ud83d\ude02\ud83d\ude02\ud83d\ude02 am sure there were no Fulanis close to that place https://t.co/9HbuJGfl4z")
    print 'pre==>', t
    d = [t]
    print normalize_corpus(d)'''
