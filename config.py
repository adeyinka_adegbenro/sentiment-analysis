import os
basedir = os.path.abspath(os.path.dirname(__file__))


class Config(object):
    DEBUG = False
    TESTING = False
    CSRF_ENABLED = True
    SECRET_KEY = '6dx7E543j2AdjKVMrcdzYbfnMfW3Ad8Y81gt2qGS'
    SQLALCHEMY_DATABASE_URI = "postgresql://formplus:formplus_dev@localhost:5432/formplus_dev"
    CONSUMER_KEY = '5A4Ca4hytUcQKB0yPOzRmJFFl'
    CONSUMER_SECRET = 'dzYbfnMfW3Ad8Y81gt2qGSnaKyoLBQNGzNVyAvtjf7b1Rt8fsH'
    ACCESS_TOKEN = '471100010-Jnd7FoXSNbTxm2AYdlFOWAJHUmT4S57wxRIh12wp'
    ACCESS_SECRET = 'iIDfwho2semphlwcpPhhnHqWbuB6dx7E543j2AdjKVMrc'


class ProductionConfig(Config):
    DEBUG = False


class StagingConfig(Config):
    DEVELOPMENT = True
    DEBUG = True


class DevelopmentConfig(Config):
    DEVELOPMENT = True
    DEBUG = True


class TestingConfig(Config):
    TESTING = True
